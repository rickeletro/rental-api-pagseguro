function verificar(token, callback) {
  const express = require('express')
  const app = express()   
  const jwt = require('jsonwebtoken')  
  const config = require('./config.js') 
    // decode token
    if (token) {  
      // verifies secret and checks exp
      app.set('superSecret', config.secret)
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
        if (err) {
          return  callback({ success: false, message: 'Failed to authenticate token.' })    
        } else {
          // if everything is good, save to request for use in other routes
          // req.decoded = decoded; 
          return callback(null, { success: true } )   
          next();
        }
      })  
    } else {  
      // if there is no token
      // return an error
      return callback({ 
          success: false, 
          message: 'No token provided.' 
      })
  
    }
  }

module.exports = {
    verificar
}
