﻿const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const config = require('./config'); // get our config file

// =======================
// configuration =========
// =======================

var port = process.env.PORT || 3003

// app.set('superSecret', config.secret)
// app.use(express.static(__dirname + '/public'))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use(morgan('dev'))

// app.disable('x-powered-by')
// app.set('etag', false)
// app.use(methodOverride());                  // simulate DELETE and PUT

// Middleware para coletar o ip do usuário
// app.use(require('request-ip').mw())

// LOADING CONTROLLERS (WITH ROUTES)
// =============================================================================

app.use('/user', require('./api/user/routes').init())
app.use('/login', require('./api/login/routes').init())
app.use('/customers', require('./api/customers/routes').init())
app.use('/totems', require('./api/totems/routes').init())
app.use('/carts', require('./api/carts/routes').init())
app.use('/rental_carts', require('./api/rental_carts/routes').init())
app.use('/price_range', require('./api/price_range/routes').init())
app.use('/externo', require('./api/externo/routes').init())
app.use('/card', require('./api/card/routes').init())

// =======================
// start the server ======
// =======================

//PAGSEGURO
app.use('/payment', require('./pagseguro/payment/routes').init())

    // =======================
    // start the server ======
    // =======================
app.listen(port)
console.log('Serviço iniciado na porta %s' + port)