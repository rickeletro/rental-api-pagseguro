var pagseguro = require('pagseguro');

var pag = new pagseguro({
                email : 'rickeletro@gmail.com',
                token: '975056428B6F45E79B79C7AFA362BDF1',
                mode : 'sandbox'
            });

//Configurando a moeda e a referência do pedido
pag.currency('BRL');
pag.reference('12345');

pag.modeType('default');

//Configuranto URLs de retorno e de notificação (Opcional)
//ver https://pagseguro.uol.com.br/v2/guia-de-integracao/finalizacao-do-pagamento.html#v2-item-redirecionando-o-comprador-para-uma-url-dinamica
pag.setNotificationURL("http://techlise.dyndns.org:3000/notificacao");
pag.setReceiverEmail("rickeletro@gmail.com");

//exportar a configuração do pagseguro
module.exports.pagCart = pag;