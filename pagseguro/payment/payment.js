var pag = require('../config/pagseguroConfig');
var parseString = require('xml2js').parseString;

function post(data, callback) {
    //Configurando as informações do comprador
    pag.pagCart.buyer({
        name: 'Emerson jose',
        email: 'c98242048797564591520@sandbox.pagseguro.com.br',
        hash: data.hash,
        phoneAreaCode: '51',
        phoneNumber: '12345678',
        CPF: '06528582602'
    });

    //Adicionar os produtos no carrinho do pagseguro    
    pag.pagCart.addItem({
        id: '0001',
        description: 'aluguel',
        amount: data.amount,
        quantity: 1
        // weight: 2342
    });

    //Configurando a entrega do pedido
    pag.pagCart.shipping({
        type: 1,
        street: 'Rua Alameda dos Anjos',
        number: '367',
        complement: 'Apto 307',
        district: 'Parque da Lagoa',
        postalCode: '01452002',
        city: 'São Paulo',
        state: 'RS',
        country: 'BRA'
    });

    pag.pagCart.payMethod({
        method: 'creditCard',
    });

    pag.pagCart.creditCard({
        token: data.tokenCard,
        quantity: 1,
        noInterestInstallmentQuantity: 2,
        value: data.amount,       
        name: 'Emerson jose', //data.name,
        birthDate: '19/09/1983',
        documenttype: 'CPF',
        documentvalue: '06528582602',
        street: 'Rua Alameda dos Anjos',
        number: '367',
        complement: 'Apto 307',
        district: 'Parque da Lagoa',
        postalCode: '01452002',
        city: 'São Paulo',
        state: 'RS',
        country: 'BRA'

    });

    pag.pagCart.bank({
        name: 'bancodobrasil'// data.bank,
    });

    
    

    //Enviando o xml ao pagseguro
    pag.pagCart.checkout(function (err, res) {
        if (err) {
            console.log(err);
        }
        var retorno = null
        parseString(res, function (err, result) {
            retorno = result
        });
        return callback(null, { success: true, result: retorno })
    });
}

function initSessionPayment(callback) {
    //Enviando o xml ao pagseguro
    pag.pagCart.session(function (err, res) {
        if (err) {
            console.log(err);
        }
        var sessionId = ''
        parseString(res, function (err, result) {
            sessionId = result.session.id[0]
        });
        return callback(null, { success: true, session: sessionId })
    });
}

module.exports = {
    post,
    initSessionPayment
}