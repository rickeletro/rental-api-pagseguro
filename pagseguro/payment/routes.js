function init() {

    const express 		   = require("express")
    const paymentController   = require("./paymentController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            paymentController.post
        )
        .get(
            paymentController.initSessionPayment
        )

    return routes

}

module.exports = {
    init
}