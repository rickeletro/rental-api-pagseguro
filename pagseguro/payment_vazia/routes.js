function init() {

    const express 		   = require("express")
    const paymentController   = require("./paymentController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            paymentController.post
        )

    return routes

}

module.exports = {
    init
}