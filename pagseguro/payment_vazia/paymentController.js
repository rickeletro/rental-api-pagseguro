const user   = require('./payment.js')


function post (req, res, next) {
  user.post (req.body, (err, data) => {
    if (err) {
      res.status(500).send(err)
    } else {
      // console.log(res)
      res.json(data)
    }
  })
}

module.exports = {
  post
}
