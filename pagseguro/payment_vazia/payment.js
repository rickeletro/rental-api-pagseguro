var pag = require('../config/pagseguroConfig');
var fetch = require('node-fetch');
var parseString = require('xml2js').parseString;
// var xml = require('jstoxml');
// var req = require('request');

const URL_TO_INIT_SECTION = 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout?email=' + pag.email + '&token=' + pag.token;
const URL_TO_FETCH = 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';

function post(data, callback) {

    

    fetch(URL_TO_INIT_SECTION, { method: 'POST', headers: {
        'X-PUBLIC-KEY-SNBH': undefined,
        'X-PRIVATE-KEY-SNBH-SALT': undefined,
        'X-PRIVATE-KEY-SNBH-RESULT': undefined,
        'Content-Type': undefined,
        'Access-Control-Allow-Methods': "GET,PUT,POST,DELETE,OPTIONS",
        'Access-Control-Allow-Headers': "Content-Type, Authorization, Content-Length, X-Requested-With",
        'Access-Control-Allow-Origin': 'https://sandbox.pagseguro.uol.com.br'
    }})
        // .then(response => response.json()) // retorna uma promise
        .then(response => {
            console.log(response);
            return callback(null, { success: true })
        })
        .catch(err => {
            // trata se alguma das promises falhar
            console.error('Failed retrieving information', err);
        }); 

    /* fetch(URL_TO_FETCH)
        .then(response => response.text()) // retorna uma promise
        .then(result => {
            console.log(result);
            return callback(null, { success: true })
        })
        .catch(err => {
            // trata se alguma das promises falhar
            console.error('Failed retrieving information', err);
        }); */

    /*parseString(res, function (err, result) {
        console.dir(result);
    }); */
    // console.log(pag.token);

    //console.dir(JSON.stringify(pag));

}

module.exports = {
    post
}