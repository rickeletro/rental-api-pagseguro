const jwt = require('jsonwebtoken')

function verifyToken(req, res, next) {
  let token = req.headers['Authorization'] || req.headers['authorization']
  if (!token) return res.status(403).json({auth: false, error: 'Não foi passado um token.'})

  jwt.verify(token, module.exports.secret, (err, decoded) => {
  if (err) return res.status(500).json({auth: false, error: 'Token não autenticado.'})

  req.decoded = decoded
  next()
  })
}

module.exports = {
    'secret'					: 'nXcd#0lI',
    'mysqlConfig'				: {
    connectionLimit : 25,
    host     : 'localhost',    
		user     : 'rental',
    password : '@W3e$R5t',
    /*
		user     : 'root',
    password : '',
    */
    database : 'RENTAL_CAR'
	},
  verifyToken
}
