const mysql = require('mysql')
const config = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {
    let pad_payment_id = null
    let pad_token = data.token
    let pad_token_card = data.tokenCard
    let pad_bin = data.bin
    let pad_card_number = data.cardNumber
    let pad_cvv = data.cvv
    let pad_expiration_month = data.expirationMonth
    let pad_expiration_year = data.expirationYear
    let pad_img_url = data.imgURL
    let pad_hash = data.hash
    let pad_bank = data.bank
    let pad_cum_customers_id = data.cum_customers_id

    let query = 'INSERT INTO pad_payment_data SET ?'
    let values = [{ pad_token: pad_token, pad_token_card: pad_token_card, pad_bin: pad_bin, pad_card_number: pad_card_number, pad_cvv: pad_cvv, pad_expiration_month: pad_expiration_month, pad_expiration_year: pad_expiration_year, pad_img_url: pad_img_url, pad_hash: pad_hash, pad_bank: pad_bank, pad_cum_customers_id: pad_cum_customers_id }]

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else {
                return callback(null, { status: 201, success: true })
            }
        })
    })

}

function putId(route, data, callback) {

    let cum_customers_id = data.cum_customers_id
    let values = ''

    switch (route) {
        case '1':
            // let pad_token = data.token
            let pad_token_card = data.tokenCard
            let pad_bin = data.bin
            // let pad_card_number = data.cardNumber
            // let pad_cvv = data.cvv
            let pad_expiration_month = data.expirationMonth
            let pad_expiration_year = data.expirationYear
            //let pad_img_url = data.imgURL
            let pad_hash = data.hash
            //let pad_bank = data.bank
            let pad_cum_customers_id = data.cum_customers_id

            // values =  [{pad_token: pad_token, pad_token_card: pad_token_card, pad_bin: pad_bin, pad_card_number: pad_card_number, pad_cvv: pad_cvv, pad_expiration_month: pad_expiration_month, pad_expiration_year: pad_expiration_year, pad_img_url: pad_img_url, pad_hash: pad_hash, pad_bank: pad_bank, pad_cum_customers_id: cum_customers_id}, {cum_customers_id: cum_customers_id}]
            values = [{ pad_token_card: pad_token_card, pad_bin: pad_bin, pad_expiration_month: pad_expiration_month, pad_expiration_year: pad_expiration_year, pad_hash: pad_hash}, { cum_customers_id: cum_customers_id }]

            break;

        case '2':

            // Altera hash de pagamento

            let pad_hash = data.hash
            values = [{ pad_hash: pad_hash }, { pad_cum_customers_id: pad_cum_customers_id }]

            break;
    }

    let query = 'UPDATE pad_payment_data SET ? WHERE ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (results.affectedRows == 0) {
                return callback(null, { status: 401, success: false, error: "Nenhum registro foi alterado." })
            } else {
                return callback(null, { status: 200, success: true })
            }
        })
    })
}

function deleteId(cum_customers_id, callback) {

    // Desativar o cliente

    let cum_status = 0

    let values = [{ cum_status: cum_status }, { cum_customers_id: cum_customers_id }]

    let query = 'UPDATE pad_payment_data SET ? WHERE ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (results.affectedRows == 0) {
                return callback(null, { status: 401, success: false, error: "Nenhum registro foi alterado." })
            } else {
                return callback(null, { status: 200, success: true })
            }
        })
    })
}

function get(callback) {

    // Listar todos os clientes

    let query = 'SELECT * FROM pad_payment_data'

    pool.getConnection((err, connection) => {
        connection.query(query, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (!results.length) {
                return callback(null, { status: 401, success: false, error: 'Nenhum registro encontrado.' })
            } else {
                var data = JSON.stringify(results)
                return callback(null, JSON.parse(data))
            }
        })
    })
}

function getId(id, callback) {
    values = [id]
    query = 'SELECT * from pad_payment_data WHERE pad_cum_customers_id = ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (!results.length) {
                return callback(null, { status: 401, success: false, error: 'Nenhum registro encontrado.' })
            } else {
                data = JSON.stringify(results)
                return callback(null, JSON.parse(data))
            }
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}