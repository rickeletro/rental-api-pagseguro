const config = require('../../config')

function init() {

    const express 		   = require("express")
    const cardController   = require("./cardController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            cardController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            cardController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            cardController.getId 
        )
        .put(
            config.verifyToken, 
            cardController.putId
        )
        .delete(
            config.verifyToken, 
            cardController.deleteId
        )

    return routes

}

module.exports = {
    init
}