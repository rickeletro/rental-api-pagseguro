const card   = require('./card.js')

function post (req, res, next) {
  card.post (req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  card.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  card.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  card.get( (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  card.getId(req.params.id, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  get,
  getId
}
