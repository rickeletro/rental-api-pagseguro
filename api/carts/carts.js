const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)

var http = require('http')

function post(data, callback) {

  // Inserir cart
  
  let car_description           = data.car_description
  let car_use_users_id_created  = data.car_use_users_id_created
  let car_rfid                  = data.car_rfid

  let query = 'INSERT INTO CAR_CARTS SET ?'
  let values = [{car_description: car_description, car_use_users_id_created: car_use_users_id_created, car_rfid: car_rfid}]
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()  
          if (error) {
            return callback({status: 500, error: error})
          } else {
            return callback(null, {status: 201, success: true})
          }
      })
  })
}

function putId(route, data, callback) {

    // Atualizar informações do cart

    let car_carts_id                        = ''
    let car_status                          = ''
    let car_tot_totems_id                   = ''
    let car_position_totems 		        = ''
    let car_rfid                            = ''

    let query = ''
    
    let values = ''

    switch (route) {
        case '1':
            // Atualizar informações do cart por ID
            
            car_carts_id                    = data.car_carts_id
            let car_description 		    = data.car_description
            car_tot_totems_id               = data.car_tot_totems_id
            car_position_totems             = data.car_position_totems
            car_rfid 		                = data.car_rfid

            values = [{car_description: car_description, car_tot_totems_id: car_tot_totems_id, car_position_totems: car_position_totems, car_rfid: car_rfid}, {car_carts_id: car_carts_id}]

            break;
            
        case '2':
            // Ativação cart
            car_carts_id                    = data.car_carts_id
            car_status 		                = 1
            
            values = [{car_status: car_status}, {car_carts_id: car_carts_id}]

            break;
            
        case '3':
            // Liberar carro

            car_carts_id                    = data.car_carts_id
    
            values = [{car_carts_id: car_carts_id}]
            query =  ' SELECT car_carts_id FROM CAR_CARTS car '
                    +' JOIN REC_RENTAL_CARTS rec ON car.car_carts_id = rec.rec_car_carts_id '
                    +' WHERE ? AND rec_status = 1 '
            
            pool.getConnection( (err, connection) => {
                connection.query(query, values, (error, results, fields) => {
                    connection.release()
                    if (error) {
                        return callback({status: 500, error: error})
                    } else if (results.length) {
                        return callback(null, {status: false, msg: "Carro alugado."})
                    } else {
                        car_status 		                = 0
                    }
                })
            })

            break;
    }

    query = 'UPDATE CAR_CARTS SET ? WHERE ?'
    
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
            } else {
                return callback(null, {status: true})
            }
        })
    })
}

function deleteId(car_carts_id, callback) {

  // Desativar o cart

  let car_status                            = 0

  
  let values = [{ car_status: car_status, car_tot_totems_id: null, car_position_totems: null}, {car_carts_id: car_carts_id}]

  let query = 'UPDATE CAR_CARTS SET ? WHERE ?'
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()
          if (error) {
            return callback({status: 500, error: error})
          } else if (results.affectedRows == 0) {
            return callback(null, {tatus: 401, success: false, error: "Nenhum registro foi alterado." } )
          } else {
            return callback(null, {status: 200, success: true})
          }
      })
  })
}

function get (callback) {

  // Listar todos os totems

  let query = 'SELECT car_carts_id, car_description, car_status, car_tot_totems_id, car_position_totems, car_rfid, car_use_users_id_created, car_created_at FROM CAR_CARTS'
  pool.getConnection( (err, connection) => {
      connection.query(query, (error, results, fields) => {
          connection.release()
          if (error) {
            return callback({status: 500, error: error})
          } else if(!results.length) {
            return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
          } else {
            var data = JSON.stringify(results)
            return callback(null, JSON.parse(data))
          }
      })
  })
}

function getId (route, param, callback) {

    // Buscar informações dos carts
    
    let values                              = ''
    let query                               = ''

    switch (route) {
        case '1':
            // Buscar informações do cart por ID

            let car_carts_id                = param.car_carts_id
            values                          = [car_carts_id]

            query = 'SELECT car_carts_id, car_description, car_status, car_tot_totems_id, car_position_totems, car_rfid, car_use_users_id_created, car_created_at FROM CAR_CARTS WHERE car_carts_id = ?'
        
        break;
        
        case '2':
            // Buscar informações dos cart ativos

            query = 'SELECT car_carts_id, car_description, car_status, car_tot_totems_id, car_position_totems, car_rfid, car_use_users_id_created, car_created_at FROM CAR_CARTS WHERE car_status = 1'
        
        break;
        
        case '3':
            // Buscar informações dos cart ativos por totem

            let car_tot_totems_id           = param.car_tot_totems_id
            values                          = [car_tot_totems_id]

            query = 'SELECT car_carts_id, car_description, car_status, car_tot_totems_id, car_position_totems, car_rfid, car_use_users_id_created, car_created_at FROM CAR_CARTS WHERE car_status = 1 AND car_tot_totems_id = ?'
        
        break;
        
        case '4':
            // Buscar qtd dos cart ativos

            query = 'SELECT count(car_carts_id) as quant FROM CAR_CARTS WHERE car_status = 1'
        
        break;
    }
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
			if (error) {
                return callback({status: 500, error: error})
			} else if(!results.length) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro encontrado."})
			} else {
				if (route == 1 || route == 4) {
					var data = JSON.stringify(results[0])
				} else {
					data = JSON.stringify(results)
				}
				return callback(null, JSON.parse(data))
			}
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}