const config = require('../../config')

function init() {

    const express 		   = require("express")
    const cartsController   = require("./cartsController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            cartsController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            cartsController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            cartsController.getId
        )
        .put(
            config.verifyToken, 
            cartsController.putId
        )
        .delete(
            config.verifyToken, 
            cartsController.deleteId
        )

    return routes

}

module.exports = {
    init
}