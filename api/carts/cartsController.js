const carts   = require('./carts.js')

function post (req, res, next) {
  carts.post (req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  carts.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)

        if (req.params.id) {
          const dataString = JSON.stringify(req.body);
            
          const options = {
            hostname: 'http://planetaanimal.dyndns.info',
            port: 3000,
            path: '/openrelay',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Content-Length': Buffer.byteLength(dataString)
            }
          };
          
          const postar = http.request(options, (res) => {
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
              console.log(`${chunk}`);
            });
            res.on('end', () => {
              //console.log('No more data in response.');
            });
          });
          
          postar.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
          });
          
          // write data to request body
          postar.write(dataString);
          postar.end();
        }
      }
  })
}

function deleteId(req, res, next){
  carts.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  carts.get( (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  carts.getId(req.params.id, req.query, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  get,
  getId
}
