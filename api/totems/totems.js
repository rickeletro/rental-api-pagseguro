const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

  // Inserir totem
  
  let tot_description           = data.tot_description
  let tot_max_num_carts         = data.tot_max_num_carts
  let tot_zip_code              = data.tot_zip_code
  let tot_address_street        = data.tot_address_street
  let tot_address_number        = data.tot_address_number
  let tot_address_district      = data.tot_address_district
  let tot_address_city          = data.tot_address_city
  let tot_address_complement    = data.tot_address_complement
  let tot_use_users_id_created  = data.tot_use_users_id_created
  
  let query = 'INSERT INTO TOT_TOTEMS SET ?'
  let values = [{tot_description: tot_description, tot_max_num_carts: tot_max_num_carts, tot_zip_code: tot_zip_code, tot_address_street: tot_address_street, tot_address_number: tot_address_number, tot_address_district: tot_address_district, tot_address_city: tot_address_city, tot_address_complement: tot_address_complement, tot_use_users_id_created: tot_use_users_id_created}]
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()  
          if (error) {
            return callback({status: 500, error: error})
          } else {
            return callback(null, {status: 201, success: true})
          }
      })
  })
}

function putId(route, data, callback) {

    // Atualizar informações do totem

    let tot_totems_id             = data.tot_totems_id

    let values = ''

    switch (route) {
        case '1':
            // Atualizar informações do totem por ID

            let tot_description           = data.tot_description
            let tot_max_num_carts         = data.tot_max_num_carts
            let tot_zip_code              = data.tot_zip_code
            let tot_address_street        = data.tot_address_street
            let tot_address_number        = data.tot_address_number
            let tot_address_district      = data.tot_address_district
            let tot_address_city          = data.tot_address_city
            let tot_address_complement    = data.tot_address_complement
            let tot_use_users_id_created  = data.tot_use_users_id_created

            values = [{tot_description: tot_description, tot_max_num_carts: tot_max_num_carts, tot_zip_code: tot_zip_code, tot_address_street: tot_address_street, tot_address_number: tot_address_number, tot_address_district: tot_address_district, tot_address_city: tot_address_city, tot_address_complement: tot_address_complement, tot_use_users_id_created: tot_use_users_id_created}, {tot_totems_id: tot_totems_id}]

            break;
        
        case '2':
            // Ativar totem por ID

            let tot_status                = 1
            
            values = [{tot_status: tot_status}, {tot_totems_id: tot_totems_id}]

            break;
    }

    let query = 'UPDATE TOT_TOTEMS SET ? WHERE ?'
    
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
				return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
            } else {
                return callback(null, {status: 200, success: true})
            }
        })
    })
}

function deleteId(tot_totems_id, callback) {

  // Desativar o totem

  let tot_status = 0
  
  let values = [{ tot_status: tot_status}, {tot_totems_id: tot_totems_id}]

  let query = 'UPDATE TOT_TOTEMS SET ? WHERE ?'
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()
          if (error) {
              return callback({status: 500, error: error})
          } else if (results.affectedRows == 0) {
              return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado." } )
          } else {
              return callback(null, {status: 200, success: true } )
          }
      })
  })
}

function get (callback) {

  // Listar todos os totems

  let query = 'SELECT tot_totems_id, tot_description, tot_max_num_carts, tot_status, tot_zip_code, tot_address_street, tot_address_number, tot_address_district, tot_address_city, tot_address_complement, tot_use_users_id_created, tot_created_at FROM TOT_TOTEMS'
  
  pool.getConnection( (err, connection) => {
      connection.query(query, (error, results, fields) => {
          connection.release()
          if (error) {
              return callback({status: 500, error: error})
          } else if(!results.length) {
              return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
          } else {
              var data = JSON.stringify(results)
              return callback(null, JSON.parse(data))
          }
      })
  })
}

function getId (route, param, callback) {
    
    let values = ''
    let query = ''

    // Buscar informações dos totems
    
    switch (route) {
        case '1':
            // Buscar informações do totem por ID

            let tot_totems_id = param.tot_totems_id
            values = [tot_totems_id]

            query = 'SELECT tot_totems_id, tot_description, tot_max_num_carts, tot_status, tot_zip_code, tot_address_street, tot_address_number, tot_address_district, tot_address_city, tot_address_complement, tot_use_users_id_created, tot_created_at from TOT_TOTEMS WHERE tot_totems_id = ?'
        
        break;
        
        case '2':
            // Buscar informações dos totems ativos

            query = 'SELECT tot_totems_id, tot_description, tot_max_num_carts, tot_status, tot_zip_code, tot_address_street, tot_address_number, tot_address_district, tot_address_city, tot_address_complement, tot_use_users_id_created, tot_created_at from TOT_TOTEMS WHERE tot_status = 1'
        
        break;
        
        case '3':
            // Buscar quant dos totems ativos

            query = 'SELECT count(tot_totems_id) as quant from TOT_TOTEMS WHERE tot_status = 1'
        
        break;
    }
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
			if (error) {
				return callback({status: 500, error: error})
			} else if(!results.length) {
				return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
			} else {
				if (route == 1 || route == 3) {
					var data = JSON.stringify(results[0])
				} else {
					data = JSON.stringify(results)
				}
				return callback(null, JSON.parse(data))
			}
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}