const config = require('../../config')

function init() {

    const express 		   = require("express")
    const totemsController   = require("./totemsController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            totemsController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            totemsController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            totemsController.getId
        )
        .put(
            config.verifyToken, 
            totemsController.putId
        )
        .delete(
            config.verifyToken, 
            totemsController.deleteId
        )

    return routes

}

module.exports = {
    init
}