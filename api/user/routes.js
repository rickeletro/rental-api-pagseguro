const config = require('../../config')

function init() {

    const express 		   = require("express")
    const userController   = require("./userController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            userController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            userController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            userController.getId
        )
        .put(
            config.verifyToken, 
            userController.putId
        )
        .delete(
            config.verifyToken, 
            userController.deleteId
        )

    return routes

}

module.exports = {
    init
}