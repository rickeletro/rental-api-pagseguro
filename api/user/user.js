const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)
var md5 = require('md5')

function post(data, callback) {

  // Inserir usuário

	let cum_cpf                   = data.cum_cpf

	if (cum_cpf.length == 11) {

		query = "SELECT cum_customers_id FROM CUM_CUSTOMERS WHERE cum_cpf = ?"
		values = [cum_cpf]
		
		pool.getConnection( (err, connection) => {
			connection.query(query, values, (error, results, fields) => {
				connection.release()
				if (error) {
					return callback({status: 500, error: error})
				} else if (results[0]) {
					return callback(null, {status: 401, success: false, error: 'CPF já cadastrado.' } )
				} else {

					let use_password              = md5(data.use_password)
					let use_grp_group_id          = data.use_grp_group_id
					let cum_name                  = data.cum_name
					let cum_email                 = data.cum_email
					let cum_gender                = data.cum_gender
					let cum_date_birth            = data.cum_date_birth
					let cum_ddd                   = data.cum_ddd
					let cum_tel                   = data.cum_tel
					let cum_zip_code              = data.cum_zip_code
					let cum_address_street        = data.cum_address_street
					let cum_address_number        = data.cum_address_number
					let cum_address_complement    = data.cum_address_complement
					let cum_address_district      = data.cum_address_district
					let cum_address_city          = data.cum_address_city
					let cum_address_uf            = data.cum_address_uf
					
					let cum_use_users_id_access   = 0
					let query = 'INSERT INTO USE_USERS SET ?'
					let values = [{use_password: use_password, use_grp_group_id: use_grp_group_id}]
					
					pool.getConnection( (err, connection) => {
						connection.query(query, values, (error, results, fields) => {
							connection.release()  
							if (error) {
								return callback({status: 500, error: error})
							} else {
								query = "SELECT LAST_INSERT_ID()"
								connection.query(query, values, (error, results, fields) => {
									connection.release()  
									if (error) {
										return callback({status: 500, error: error})
									} else {
										cum_use_users_id_access = results[0]['LAST_INSERT_ID()']
										query = 'INSERT INTO CUM_CUSTOMERS SET ?'
										values = [{cum_cpf: cum_cpf, cum_name: cum_name, cum_email: cum_email, cum_gender: cum_gender, cum_date_birth: cum_date_birth, cum_ddd: cum_ddd, cum_tel: cum_tel, cum_zip_code: cum_zip_code, cum_address_street: cum_address_street, cum_address_number: cum_address_number, cum_address_complement: cum_address_complement, cum_address_district: cum_address_district, cum_address_city: cum_address_city, cum_address_uf: cum_address_uf, cum_use_users_id_access: cum_use_users_id_access}]
										connection.query(query, values, (error, results, fields) => {
											connection.release()  
											if (error) {
												return callback({status: 500, error: error})
											}else{
												return callback(null, {status: 201, success: true})
											}
										})
									}
								})
							}
						})
					})
				}
			})
		})
	} else {
		return callback(null, {status: 401, success: false, error: 'CPF fora do formato padrão.' })
	}
}

function putId(route, data, callback) {

	// Atualizar informações do usuário por ID

	let query = 'UPDATE USE_USERS SET ? WHERE ?'
	let values = ''

	let use_users_id                = data.use_users_id

	switch (route) {
		case '1':
			// Alterar informaçoes do usuário
			
			let use_grp_group_id    = data.use_grp_group_id

			values = [{use_grp_group_id: use_grp_group_id}, {use_users_id: use_users_id}]
		break;
		
		case '2':
			// Alterar a senha do usuário
			
			let use_password        = md5(data.use_password)

			values = [{use_password: use_password}, {use_users_id: use_users_id}]
		break;
				
		case '3':
			// Ativa o usuário
			
			let use_status          = 1

			values = [{use_status: use_status}, {use_users_id: use_users_id}]
		break;
	}
	
	pool.getConnection( (err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
            if (error) {
				return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
            } else {
                return callback(null, {status: 200, success: true})
            }
		})
	})
}

function deleteId(use_users_id, callback) {

  // Desativar o usuário
  
  let use_status = 0
  
  let values = [{ use_status: use_status}, {use_users_id: use_users_id}]
  
  let query = 'UPDATE USE_USERS SET ? WHERE ?'
	
	pool.getConnection( (err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
            if (error) {
				return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado." } )
            } else {
                return callback(null, {status: 200, success: true } )
            }
		})
	})
}

function get (callback) {

  // Listar todos os usuários
  
  let query = 'SELECT use_users_id, use_status, use_grp_group_id, use_users_id_created, use_created_at FROM USE_USERS'
  
  pool.getConnection( (err, connection) => {
		connection.query(query, (error, results, fields) => {
			connection.release()
			if (error) {
				return callback({status: 500, error: error})
			} else if(!results.length) {
				return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
			} else {
				var data = JSON.stringify(results)
				return callback(null, JSON.parse(data))
			}
		})
  })
}

function getId (route, param, callback) {

    // Buscar informações do usuário

    let query = ''
    let values = ''

    switch (route) {
			case '1':
				// Busca usuário por id
				
				let use_users_id = param.use_users_id

				values = [use_users_id]

				query = 'SELECT use_users_id, use_status, use_grp_group_id, use_users_id_created, use_created_at FROM USE_USERS WHERE use_users_id = ?'
			
			break;
					
			case '2':
				// Busca usuários ativos
				
				values = ''
				
				query = 'SELECT use_users_id, use_status, use_grp_group_id, use_users_id_created, use_created_at FROM USE_USERS WHERE use_status = 1'
			
			break;
					
			case '3':
				// Busca usuários ativos por grupo
				
				let use_grp_group_id = param.use_grp_group_id
				
				values = [use_grp_group_id]
				
				query = 'SELECT use_users_id, use_status, use_grp_group_id, use_users_id_created, use_created_at FROM USE_USERS WHERE use_status = 1 AND use_grp_group_id = ?'
				
			break;
	}
	
	pool.getConnection( (err, connection) => {
		connection.query(query, values, (error, results, fields) => {
			connection.release()
			if (error) {
				return callback({status: 500, error: error})
			} else if(!results.length) {
				return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
			} else {
				if (route == 1) {
					var data = JSON.stringify(results[0])
				} else {
					data = JSON.stringify(results)
				}
				return callback(null, JSON.parse(data))
			}
		})
	})
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}