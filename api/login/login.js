const mysql = require('mysql')
const express = require('express')
const app = express()
const jwt = require('jsonwebtoken')
const config = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)
var md5 = require('md5');

function post (data, callback) {

  let usu           = data.key
  let use_password  = data.use_password

  let query = 'SELECT usr.use_users_id, usr.use_password, usr.use_status FROM USE_USERS usr JOIN CUM_CUSTOMERS cum ON usr.use_users_id = cum.cum_use_users_id_access WHERE ( ? OR ? )'
  let values = [{'cum.cum_email': usu}, {'cum.cum_cpf': usu}]
  
  pool.getConnection( (err, connection) => {
    if (err) return callback({code: 500, error: 'Não foi possível estabelecer uma conexão com o banco de dados.'})
    connection.query(query, values, (error, results, fields) => {
      connection.release()
      if (error) {
        return callback({code: 500, error: error})
      } else if (results.length) {
        let data = results[0]
        
        const passwordIsValid = (md5(use_password) == data.use_password)
        
        if (!passwordIsValid) {
          return callback({code: 401, status: false, error: 'Usuário e/ou Senha inválido(s).'})
        } else if (data.use_status) {
          app.set('superSecret', config.secret)
          var token = jwt.sign({use_users_id: data.use_users_id}, app.get('superSecret'), {
            expiresIn: 60*60*24 //o token irá expirar em 24 horas
          })
          return callback(null, {status: true, token})
        } else {
          return callback({code: 401, status: false, error: 'Usuário inativo, entre em contato com a administração.'})
        }
      } else {
        return callback({code: 401, status: false, error: 'Usuário não cadastrado.'})
      }
    })
  })
}

function get(token, callback) {
  if (!token) {
    return callback({code: 401, status: false, error: 'Não foi passado um token.'})
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return callback({code: 500, status: false, error: 'Token não autenticado.'})
    } else {
      let query = 'SELECT usr.use_users_id, usr.use_status, usr.use_grp_group_id, cum.cum_customers_id FROM USE_USERS usr JOIN CUM_CUSTOMERS cum ON usr.use_users_id = cum.cum_use_users_id_access WHERE usr.use_users_id = ?'
      values = [decoded.use_users_id]
      
      pool.getConnection( (err, connection) => {
          connection.query(query, values, (error, results, fields) => {
              connection.release()
              if (error) {
                return callback({code: 500, status: false, error: error})
              } else if(results.length) {
                var data = JSON.stringify(results[0])
                console.log(data)
                return callback(null, JSON.parse(data))
              } else {
                return callback({code: 401, status: false, error: 'Nenhum registro foi encontrado.'})
              }
          })
      })
    }
  })
}

module.exports = {
  post,
  get
}