const login   = require('./login.js')
const verify   = require('../../verify.js')

function post (req, res, next) {
  login.post(req.body, (err, data) => {
    if (err) {
      res.status(err.code).send(err)
    } else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  const token = req.headers['Authorization'] || req.headers['authorization']
  login.get(token, (err, data) => {
    if (err) {
      res.status(err.code).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  get
}