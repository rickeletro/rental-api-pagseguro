const config = require('../../config')

function init() {

    const express 		    = require("express")
    const loginController   = require("./loginController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            loginController.post
        )
        .get(
            loginController.get
        )

    return routes

}

module.exports = {
    init
}
