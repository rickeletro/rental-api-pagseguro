const config = require('../../config')

function init() {

    const express 		   = require("express")
    const price_rangeController   = require("./price_rangeController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            price_rangeController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            price_rangeController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            price_rangeController.getId 
        )
        .put(
            config.verifyToken, 
            price_rangeController.putId
        )
        .delete(
            config.verifyToken, 
            price_rangeController.deleteId
        )

    return routes

}

module.exports = {
    init
}