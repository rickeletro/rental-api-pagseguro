const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

  // Inserir preço
  
  let prr_range_init            = data.prr_range_init
  let prr_range_end             = data.prr_range_end
  let prr_price                 = data.prr_price
  let prr_use_users_id_created  = data.prr_use_users_id_created

  let query = 'INSERT INTO PRR_PRICE_RANGE SET ?'
  let values = [{prr_range_init: prr_range_init, prr_range_end: prr_range_end, prr_price: prr_price, prr_use_users_id_created: prr_use_users_id_created}]
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()  
          if (error) {
              return callback({status: 500, error: error})
          }else{
              return callback(null, {status: 201, success: true})
          }
      })
  })
}

function putId(prr_price_range_id, data, callback) {
    
    let prr_range_init              = data.prr_range_init
    let prr_range_end               = data.prr_range_end
    let prr_price                   = data.prr_price
    let prr_status                  = data.prr_status
    let prr_date_inactivated        = data.prr_date_inactivated

    // Atualizar informações do price por ID

    let query = 'UPDATE PRR_PRICE_RANGE SET ? WHERE ?'
    let values =  [{prr_range_init: prr_range_init, prr_range_end: prr_range_end, prr_price: prr_price, prr_status: prr_status, prr_date_inactivated: prr_date_inactivated}, {prr_price_range_id: prr_price_range_id}]
    
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
				return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
            } else {
                return callback(null, {status: 200, success: true})
            }
        })
    })
}

function deleteId(prr_price_range_id, callback) {

  // Desativar o price
  
  let prr_status = 0
  
  let values = [{prr_status: prr_status}, {prr_price_range_id: prr_price_range_id}]
  
  let query = 'UPDATE PRR_PRICE_RANGE SET ? WHERE ?'
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()
          if (error) {
              return callback({status: 500, error: error})
          } else if (results.affectedRows == 0) {
              return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado." } )
          } else {
              return callback(null, {status: 200, success: true } )
          }
      })
  })
}

function get (callback) {

  // Listar todos os price

  let query = 'SELECT prr_price_range_id, prr_range_init, prr_range_end, prr_price, prr_status, prr_date_inactivated, prr_use_users_id_created, prr_created_at FROM PRR_PRICE_RANGE'

  pool.getConnection( (err, connection) => {
      connection.query(query, (error, results, fields) => {
          connection.release()
          if (error) {
              return callback({status: 500, error: error})
          } else if(!results.length) {
              return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
          } else {
              var data = JSON.stringify(results)
              return callback(null, JSON.parse(data))
          }
      })
  })
}

function getId (route, param, callback) {

    // Buscar informações

    let query = ''
    let values = ''
    var data = ''

    switch (route) {
        case '1':
            // Busca price por id
            
            let prr_price_range_id = param.prr_price_range_id
            values = [prr_price_range_id]
            query = 'SELECT prr_price_range_id, prr_range_init, prr_range_end, prr_price, prr_status, prr_date_inactivated, prr_use_users_id_created, prr_created_at FROM PRR_PRICE_RANGE WHERE prr_price_range_id = ?'
            
            break;
            
        case '2':
            // Busca price ativos
            
            values = ''
            query = 'SELECT prr_price_range_id, prr_range_init, prr_range_end, prr_price, prr_status, prr_date_inactivated, prr_use_users_id_created, prr_created_at FROM PRR_PRICE_RANGE WHERE prr_status = 1'
            
            break;
        
        case '3':
            // Busca price com data de inativação menor que
            
            let prr_date_inactivated = param.prr_date_inactivated
            values = [prr_date_inactivated]
            query = 'SELECT prr_price_range_id, prr_range_init, prr_range_end, prr_price, prr_status, prr_date_inactivated, prr_use_users_id_created, prr_created_at FROM PRR_PRICE_RANGE WHERE prr_date_inactivated < ?'
            
            break;
    }

    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
			if (error) {
				return callback({status: 500, error: error})
			} else if(!results.length) {
				return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
			} else {
				if (route == 1) {
					data = JSON.stringify(results[0])
				} else {
					data = JSON.stringify(results)
				}
				return callback(null, JSON.parse(data))
			}
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}