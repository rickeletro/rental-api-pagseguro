const price_range   = require('./price_range.js')

function post (req, res, next) {
  price_range.post (req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  price_range.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  price_range.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  price_range.get( (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  price_range.getId(req.params.id, req.query, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  get,
  getId
}
