const config = require('../../config')

function init() {

    const express 		   = require("express")
    const customersController   = require("./customersController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            customersController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            customersController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            customersController.getId 
        )
        .put(
            config.verifyToken, 
            customersController.putId
        )
        .delete(
            config.verifyToken, 
            customersController.deleteId
        )

    return routes

}

module.exports = {
    init
}