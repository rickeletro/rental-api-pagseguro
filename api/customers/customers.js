const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

  // Inserir cliente
  
  let cum_cpf                   = data.cum_cpf

  if (cum_cpf.length == 11) {

		query = "SELECT cum_customers_id FROM CUM_CUSTOMERS WHERE cum_cpf = ?"
		values = [cum_cpf]
		
		pool.getConnection( (err, connection) => {
			connection.query(query, values, (error, results, fields) => {
				if (error) {
					connection.release()
					return callback({status: 500, error: error})
				} else if (results[0]) {
					return callback(null, {status: 401, success: false, status: 'CPF já cadastrado.'})
				} else {

					let cum_name                  = data.cum_name
					let cum_email                 = data.cum_email
					let cum_gender                = data.cum_gender
					let cum_date_birth            = data.cum_date_birth
					let cum_ddd                   = data.cum_ddd
					let cum_tel                   = data.cum_tel
					let cum_zip_code              = data.cum_zip_code
					let cum_address_street        = data.cum_address_street
					let cum_address_number        = data.cum_address_number
					let cum_address_complement    = data.cum_address_complement
					let cum_address_district      = data.cum_address_district
					let cum_address_city          = data.cum_address_city
					let cum_address_uf            = data.cum_address_uf
					let cum_use_users_id_access   = data.cum_use_users_id_access
					let cum_use_users_id_created  = data.cum_use_users_id_created

					let query = 'INSERT INTO CUM_CUSTOMERS SET ?'
					let values = [{cum_cpf: cum_cpf, cum_name: cum_name, cum_email: cum_email, cum_gender: cum_gender, cum_date_birth: cum_date_birth, cum_ddd: cum_ddd, cum_tel: cum_tel, cum_zip_code: cum_zip_code, cum_address_street: cum_address_street, cum_address_number: cum_address_number, cum_address_complement: cum_address_complement, cum_address_district: cum_address_district, cum_address_city: cum_address_city, cum_address_uf: cum_address_uf, cum_use_users_id_access: cum_use_users_id_access, cum_use_users_id_created: cum_use_users_id_created}]
					
					pool.getConnection( (err, connection) => {
                        connection.query(query, values, (error, results, fields) => {
                            connection.release()  
                            if (error) {
                                return callback({status: 500, error: error})
                            } else {
                                return callback(null, {status: 201, success: true})
                            }
                        })
					})
				}
			})
		})
	} else {
		return callback(null, {status: 401, success: false, error: 'CPF fora do formato padrão.'})
	}
}

function putId(route, data, callback) {

    let cum_customers_id                  = data.cum_customers_id
    let values = ''

    switch (route) {
        case '1':
        
            // Atualizar informações do cliente por ID

            let cum_name                  = data.cum_name
            let cum_email                 = data.cum_email
            let cum_gender                = data.cum_gender
            let cum_date_birth            = data.cum_date_birth
            let cum_ddd                   = data.cum_ddd
            let cum_tel                   = data.cum_tel
            let cum_zip_code              = data.cum_zip_code
            let cum_address_street        = data.cum_address_street
            let cum_address_number        = data.cum_address_number
            let cum_address_complement    = data.cum_address_complement
            let cum_address_district      = data.cum_address_district
            let cum_address_city          = data.cum_address_city
            let cum_address_uf            = data.cum_address_uf

            values =  [{cum_name: cum_name, cum_email: cum_email, cum_gender: cum_gender, cum_date_birth: cum_date_birth, cum_ddd: cum_ddd, cum_tel: cum_tel, cum_zip_code: cum_zip_code, cum_address_street: cum_address_street, cum_address_number: cum_address_number, cum_address_district: cum_address_district, cum_address_city: cum_address_city, cum_address_complement: cum_address_complement, cum_address_uf: cum_address_uf}, {cum_customers_id: cum_customers_id}]
        
        break;
        
        case '2':
        
            // Altera status cliente
        
            let cum_status                = 1
            values = [{cum_status: cum_status}, {cum_customers_id: cum_customers_id}]
        
        break;
    }

    let query = 'UPDATE CUM_CUSTOMERS SET ? WHERE ?'
    
    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({status: 500, error: error})
            } else if (results.affectedRows == 0) {
                return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
            } else {
                return callback(null, {status: 200, success: true})
            }
        })
    })
}

function deleteId(cum_customers_id, callback) {

  // Desativar o cliente
  
  let cum_status = 0
  
  let values = [{ cum_status: cum_status}, {cum_customers_id: cum_customers_id}]
  
  let query = 'UPDATE CUM_CUSTOMERS SET ? WHERE ?'
  
  pool.getConnection( (err, connection) => {
      connection.query(query, values, (error, results, fields) => {
          connection.release()
          if (error) {
            return callback({status: 500, error: error})
          } else if (results.affectedRows == 0) {
              return callback(null, {status: 401, success: false, error: "Nenhum registro foi alterado."})
          } else {
              return callback(null, {status: 200, success: true } )
          }
      })
  })
}

function get (callback) {

  // Listar todos os clientes

  let query = 'SELECT cum_customers_id, cum_cpf, cum_name, cum_email, cum_gender, cum_date_birth, cum_ddd, cum_tel, cum_zip_code, cum_address_street, cum_address_number, cum_address_district, cum_address_city, cum_address_complement, cum_address_uf, cum_status, cum_use_users_id_access, cum_use_users_id_created, cum_created_at FROM CUM_CUSTOMERS'

  pool.getConnection( (err, connection) => {
      connection.query(query, (error, results, fields) => {
          connection.release()
          if (error) {
            return callback({status: 500, error: error})
          } else if(!results.length) {
              return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
          } else {
              var data = JSON.stringify(results)
              return callback(null, JSON.parse(data))
          }
      })
  })
}

function getId (route, param, callback) {

    // Buscar informações do cliente

    let query = ''
    let values = ''

    switch (route) {
        case '1':
            // Busca cliente por id usuário
            
            let cum_use_users_id_access = param.cum_use_users_id_access
            values = [cum_use_users_id_access]
            query = 'SELECT cum_customers_id, cum_cpf, cum_name, cum_email, cum_gender, cum_date_birth, cum_ddd, cum_tel, cum_zip_code, cum_address_street, cum_address_number, cum_address_district, cum_address_city, cum_address_complement, cum_address_uf, cum_status, cum_use_users_id_access, cum_use_users_id_created, cum_created_at from CUM_CUSTOMERS WHERE cum_use_users_id_access = ?'

            break;
        
        case '2':
            // Busca cliente por id
            
            let cum_customers_id = param.cum_customers_id
            values = [cum_customers_id]
            query = 'SELECT cum_customers_id, cum_cpf, cum_name, cum_email, cum_gender, cum_date_birth, cum_ddd, cum_tel, cum_zip_code, cum_address_street, cum_address_number, cum_address_district, cum_address_city, cum_address_complement, cum_address_uf, cum_status, cum_use_users_id_access, cum_use_users_id_created, cum_created_at from CUM_CUSTOMERS WHERE cum_customers_id = ?'
            
            break;
            
        case '3':
            // Busca clientes ativos
            
            values = ''
            query = 'SELECT cum_customers_id, cum_cpf, cum_name, cum_email, cum_gender, cum_date_birth, cum_ddd, cum_tel, cum_zip_code, cum_address_street, cum_address_number, cum_address_district, cum_address_city, cum_address_complement, cum_address_uf, cum_status, cum_use_users_id_access, cum_use_users_id_created, cum_created_at from CUM_CUSTOMERS WHERE cum_status = 1'
            
            break;
        
        case '4':
            // Busca cliente por cum_cpf
            
            let cum_cpf = param.cum_cpf
            values = [cum_cpf]
            query = 'SELECT cum_customers_id, cum_cpf, cum_name, cum_email, cum_gender, cum_date_birth, cum_ddd, cum_tel, cum_zip_code, cum_address_street, cum_address_number, cum_address_district, cum_address_city, cum_address_complement, cum_address_uf, cum_status, cum_use_users_id_access, cum_use_users_id_created, cum_created_at from CUM_CUSTOMERS WHERE cum_cpf = ?'
            
            break;
            
        case '5':
            // Busca quantidade clientes ativos
            
            values = ''
            query = 'SELECT count(cum_customers_id) as quant from CUM_CUSTOMERS WHERE cum_status = 1'
            
            break;
    }

    pool.getConnection( (err, connection) => {
        connection.query(query, values, (error, results, fields) => {
					console.log(results.length)
            connection.release()
			if (error) {
                return callback({status: 500, error: error})
			} else if(!results.length) {
				return callback(null, {status: 401, success: false, error: 'Nenhum registro encontrado.'})
			} else {
				if (route == 1 || route == 2 || route == 4 || route == 5) {
					var data = JSON.stringify(results[0])
				} else {
					data = JSON.stringify(results)
				}
				return callback(null, JSON.parse(data))
			}
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId
}