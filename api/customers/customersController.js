const customers   = require('./customers.js')

function post (req, res, next) {
  customers.post (req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  customers.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  customers.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  customers.get( (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  customers.getId(req.params.id, req.query, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  get,
  getId
}
