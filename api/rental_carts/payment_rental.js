const pagseguro = require('pagseguro');

const pag = new pagseguro({
    email: 'rickeletro@gmail.com',
    token: '975056428B6F45E79B79C7AFA362BDF1',
    mode: 'sandbox'
});
//Configurando a moeda e a referência do pedido
pag.currency('BRL');
pag.reference('12345');
pag.modeType('default');
//Configuranto URLs de retorno e de notificação (Opcional)
pag.setNotificationURL("http://techlise.dyndns.org:3000/notificacao");
pag.setReceiverEmail("rickeletro@gmail.com");

var parseString = require('xml2js').parseString;

function payment(rfid) {
    //Configurando as informações do comprador
    pag.buyer({
        name: 'Emerson jose',
        email: 'c98242048797564591520@sandbox.pagseguro.com.br',
        hash: data.hash,
        phoneAreaCode: '51',
        phoneNumber: '12345678',
        CPF: '06528582602'
    });

    //Adicionar os produtos no carrinho do pagseguro    
    pag.addItem({
        id: '0001',
        description: 'aluguel',
        amount: data.amount,
        quantity: 1
        // weight: 2342
    });

    //Configurando a entrega do pedido
    pag.shipping({
        type: 1,
        street: 'Rua Alameda dos Anjos',
        number: '367',
        complement: 'Apto 307',
        district: 'Parque da Lagoa',
        postalCode: '01452002',
        city: 'São Paulo',
        state: 'RS',
        country: 'BRA'
    });

    pag.payMethod({
        method: 'creditCard',
    });

    pag.creditCard({
        token: data.tokenCard,
        quantity: 1,
        noInterestInstallmentQuantity: 2,
        value: data.amount,
        name: 'Emerson jose', //data.name,
        birthDate: '19/09/1983',
        documenttype: 'CPF',
        documentvalue: '06528582602',
        street: 'Rua Alameda dos Anjos',
        number: '367',
        complement: 'Apto 307',
        district: 'Parque da Lagoa',
        postalCode: '01452002',
        city: 'São Paulo',
        state: 'RS',
        country: 'BRA'

    });

    pag.bank({
        name: 'bancodobrasil'// data.bank,
    });

    //Enviando o xml ao pagseguro
    pag.checkout(function (err, res) {
        if (err) {
            console.log(err);
        }
        var retorno = null
        parseString(res, function (err, result) {
            retorno = result
        });
        console.log(retorno)
        // return callback(null, { success: true, result: retorno })
    });
}


module.exports = {
    payment
}