const mysql = require('mysql')
const config = require('../../config.js')
const payment_rental   = require('./payment_rental.js')
var pool = mysql.createPool(config.mysqlConfig)

function post(data, callback) {

    // Inserir aluguel

    let rec_car_carts_id = data.rec_car_carts_id
    let rec_cum_customers_id = data.rec_cum_customers_id
    let pad_hash = data.hash

    let values = [{ rec_car_carts_id: rec_car_carts_id, rec_cum_customers_id: rec_cum_customers_id }]
    let query = 'INSERT INTO REC_RENTAL_CARTS SET ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else {
                // Atualiza um hash de pagamento para efetuar o pagamento na entrega
                query = 'UPDATE pad_payment_data SET ? WHERE ?'
                values = [{ pad_hash: pad_hash }, { pad_cum_customers_id: rec_cum_customers_id }]
                pool.getConnection((err, connection) => {
                    connection.query(query, values, (error, results, fields) => {
                        connection.release()
                        if (error) {
                            return callback({ status: 500, error: error })
                        } else if (results.affectedRows == 0) {
                            return callback(null, { status: 401, success: false, error: "Nenhum registro foi alterado." })
                        } else {
                            // return callback(null, { status: 200, success: true })
                            return callback(null, { status: true })
                        }
                    })
                })
            }
        })
    })
}

function putId(car_rfid, data, callback) {
    
    // Atualizar hora termino do aluguel
    let car_position_totems = data.car_position_totems
    let car_tot_totems_id = data.car_tot_totems_id

    let values = ''
    let query = ''

    values = [{ car_rfid: car_rfid }]
    query = ' SELECT car_carts_id FROM CAR_CARTS car '
        + ' JOIN REC_RENTAL_CARTS rec ON car.car_carts_id = rec.rec_car_carts_id '
        + ' WHERE ? AND rec_status = 1 '

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else {
                if (results.length) {
                    values = [car_position_totems, car_tot_totems_id, car_rfid]

                    query = ' UPDATE CAR_CARTS car '
                        + ' JOIN REC_RENTAL_CARTS rec ON car.car_carts_id = rec.rec_car_carts_id '
                        + ' SET '
                        + ' car.car_status = 1, '
                        + ' car.car_position_totems = ?, '
                        + ' car.car_tot_totems_id = ?, '
                        + ' rec.rec_rental_end = now(), '
                        + ' rec.rec_rental_time = TIME_TO_SEC(TIMEDIFF(now(),rec.rec_rental_start))/60, '
                        + ' rec.rec_status = 2 '
                        + ' WHERE car.car_rfid = ? '
                        + '   AND rec.rec_status = 1 '

                    pool.getConnection((err, connection) => {
                        connection.query(query, values, (error, results, fields) => {
                            connection.release()
                            if (error) {
                                return callback({ status: 500, error: error })
                            } else if (results.affectedRows == 0) {
                                return callback(null, { status: false, error: "Nenhum aluguel encontrado." })
                            } else {
                                //Executa o pagamento
                                payment_rental.payment(car_rfid)
                                return callback(null, { status: true })
                            }
                        })
                    })
                } else {
                    values = [car_position_totems, car_tot_totems_id, car_rfid]

                    query = ' UPDATE CAR_CARTS car '
                        + ' SET '
                        + ' car_status = 1, '
                        + ' car_position_totems = ?, '
                        + ' car_tot_totems_id = ? '
                        + ' WHERE car.car_rfid = ? '
                        + '   AND car_tot_totems_id is null '
                        + '   AND car_position_totems is null '

                    pool.getConnection((err, connection) => {
                        connection.query(query, values, (error, results, fields) => {
                            connection.release()
                            if (error) {
                                return callback({ status: 500, error: error })
                            } else if (results.affectedRows == 0) {
                                return callback(null, { status: false, error: "Nenhum registro foi alterado." })
                            } else {
                                return callback(null, { status: true })
                            }
                        })
                    })
                }
            }
        })
    })
}

function deleteId(rec_rental_carts_id, callback) {

    // Desativar o aluguel

    let rec_status = 0

    let values = [{ rec_status: rec_status }, { rec_rental_carts_id: rec_rental_carts_id }]

    let query = 'UPDATE REC_RENTAL_CARTS SET ? WHERE ?'

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (results.affectedRows == 0) {
                return callback(null, { status: 401, success: false, error: "Nenhum registro foi alterado." })
            } else {
                return callback(null, { status: 200, success: true })
            }
        })
    })
}

function get(callback) {

    // Listar todos os alugueis

    let query = 'SELECT rec_rental_carts_id, rec_car_carts_id, rec_cum_customers_id, rec_rental_start, rec_rental_end, rec_rental_time, rec_prr_price_range_id, rec_status FROM REC_RENTAL_CARTS'

    pool.getConnection((err, connection) => {
        connection.query(query, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else if (!results.length) {
                return callback(null, { status: 401, success: false, error: 'Nenhum registro encontrado.' })
            } else {
                var data = JSON.stringify(results)
                return callback(null, JSON.parse(data))
            }
        })
    })
}

function getId(route, param, callback) {

    // Buscar informações dos alugueis

    let rec_cum_customers_id = param.rec_cum_customers_id

    let values = ''
    let query = ''

    switch (route) {
        case '1':
            // Buscar informações do aluguel por ID

            let rec_rental_carts_id = param.rec_rental_carts_id
            values = [{ rec_rental_carts_id: rec_rental_carts_id }]

            query = 'SELECT rec_rental_carts_id, rec_cum_customers_id, rec_car_carts_id, rec_rental_start, rec_rental_end, rec_rental_time, rec_prr_price_range_id, rec_status FROM REC_RENTAL_CARTS WHERE rec_carts_id = ?'

            break;

        case '2':
            // Buscar informações dos alugueis em aberto

            query = 'SELECT rec_rental_carts_id, rec_cum_customers_id, rec_car_carts_id, rec_rental_start, rec_rental_end, rec_rental_time, rec_prr_price_range_id, rec_status FROM REC_RENTAL_CARTS WHERE rec_rental_end IS NULL'

            break;

        case '3':
            // Buscar informações dos alugueis por cliente

            values = [{ rec_cum_customers_id: rec_cum_customers_id }]

            query = 'SELECT rec_rental_carts_id, rec_cum_customers_id, rec_car_carts_id, rec_rental_start, rec_rental_end, rec_rental_time, rec_prr_price_range_id, rec_status FROM REC_RENTAL_CARTS WHERE rec_cum_customers_id = ?'

            break;

        case '4':
            // Buscar informações dos alugueis em aberto por cliente

            values = [{ rec_cum_customers_id: rec_cum_customers_id }]

            query = `SELECT	rec_rental_carts_id, rec_cum_customers_id, rec_car_carts_id, car_description, rec_rental_start, DATE_FORMAT(rec_rental_start, '%d/%m/%Y') as 'data', TIME_FORMAT(rec_rental_start, '%H:%i') as 'hora', rec_rental_end, rec_rental_time, rec_prr_price_range_id, rec_status FROM REC_RENTAL_CARTS rec JOIN CAR_CARTS car ON rec.rec_car_carts_id = car.car_carts_id WHERE rec_cum_customers_id = ? AND rec_rental_end IS NULL`

            break;

        case '5':
            // Buscar quantidade dos alugueis em aberto

            query = 'SELECT count(rec_rental_carts_id) as quant FROM REC_RENTAL_CARTS WHERE rec_rental_end is null'

            break;
    }
    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ code: 500, error: error })
            } else if (!results.length) {
                return callback(null, { status: false, error: 'Nenhum registro encontrado.' })
            } else {
                if (route == 1 || route == 5) {
                    var data = JSON.stringify(results[0])
                } else {
                    data = JSON.stringify(results)
                }
                return callback(null, JSON.parse(data))
            }
        })
    })
}

function checkAvailability(car_carts_id, callback) {

    let values = [{ car_carts_id: car_carts_id }]
    let query = ' SELECT car_carts_id FROM CAR_CARTS car '
        + ' JOIN REC_RENTAL_CARTS rec ON car.car_carts_id = rec.rec_car_carts_id '
        + ' WHERE ? AND rec_status = 1 '

    pool.getConnection((err, connection) => {
        connection.query(query, values, (error, results, fields) => {
            connection.release()
            if (error) {
                return callback({ status: 500, error: error })
            } else {
                if (results.length) {
                    return callback(null, { status: false, msg: 'Carro já alugado' })
                } else {
                    return callback(null, { status: true })
                }
            }
        })
    })
}

module.exports = {
    post,
    putId,
    deleteId,
    get,
    getId,
    checkAvailability
}