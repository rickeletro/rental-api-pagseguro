const rental_carts   = require('./rental_carts.js')

// Add
var http = require('http')

function post (req, res, next) {
  
  car_carts_id = req.body.rec_car_carts_id

  rental_carts.checkAvailability(car_carts_id, (err, data) => {

    if (err) {
      res.status(err.status).send(err)
    } else {
      if (data.status) {
        rental_carts.post (req.body, (err, data) => {
          
          if (err) {
            res.status(err.status).send(err)
          } else {
            res.json(data)
            
            const dataString = JSON.stringify(req.body);
          
            const options = {
              hostname: 'http://planetaanimal.dyndns.info',
              port: 3000,
              path: '/openrelay',
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(dataString)
              }
            };
            
            const postar = http.request(options, (res) => {
              res.setEncoding('utf8');
              res.on('data', (chunk) => {
                console.log(`${chunk}`);
              });
              res.on('end', () => {
                //console.log('No more data in response.');
              });
            });
            
            postar.on('error', (e) => {
              console.error(`problem with request: ${e.message}`);
            });
            
            // write data to request body
            postar.write(dataString);
            postar.end();
      
          }
        })
      } else {
        res.json(data)
      }
    }

  })
}

function putId (req, res, next) {
  rental_carts.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  rental_carts.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  rental_carts.get( (err, content) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(content)
    }
  })
}

function getId (req, res, next) {
  rental_carts.getId(req.params.id, req.query, (err, content) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(content)
    }
  })
}

module.exports = {
  post,
  putId,
  deleteId,
  get,
  getId
}
