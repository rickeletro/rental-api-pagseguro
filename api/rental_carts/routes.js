const config = require('../../config')

function init() {

    const express 		            = require("express")
    const rental_cartsController    = require("./rental_cartsController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            config.verifyToken, 
            rental_cartsController.post
        )

    routes.route('/')
        .get(
            config.verifyToken, 
            rental_cartsController.get
        )
        
    routes.route('/:id/')
        .get(
            config.verifyToken, 
            rental_cartsController.getId
        )
        .put(
            rental_cartsController.putId
        )
        .delete(
            config.verifyToken, 
            rental_cartsController.deleteId
        )

    return routes

}

module.exports = {
    init
}