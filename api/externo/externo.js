const mysql           = require('mysql')
const config          = require('../../config.js')

var pool = mysql.createPool(config.mysqlConfig)
var md5 = require('md5')

function post(data, callback) {
	return true
}

function postId(route, data, callback) {

	let query = ''
	let values = ''

	switch (route) {
		case '1':
			// Inserir cliente

			let cum_cpf                   = data.cum_cpf

			if (cum_cpf.length == 11) {

				query = "SELECT cum_customers_id FROM CUM_CUSTOMERS WHERE cum_cpf = ?"
				values = [cum_cpf]
				
				pool.getConnection( (err, connection) => {
					connection.query(query, values, (error, results, fields) => {
						if (error) {
							connection.release()
							return callback({status: 500, error: error})
						} else if (results[0]) {
							return callback(null, {status: 401, success: false, error: 'CPF já cadastrado.'})
						} else {

							let use_password              = md5(data.use_password)
							let use_grp_group_id          = data.use_grp_group_id
							let cum_name                  = data.cum_name
							let cum_email                 = data.cum_email
							let cum_gender                = data.cum_gender
							let cum_date_birth            = data.cum_date_birth
							let cum_ddd                   = data.cum_ddd
							let cum_tel                   = data.cum_tel
							let cum_zip_code              = data.cum_zip_code
							let cum_address_street        = data.cum_address_street
							let cum_address_number        = data.cum_address_number
							let cum_address_complement    = data.cum_address_complement
							let cum_address_district      = data.cum_address_district
							let cum_address_city          = data.cum_address_city
							let cum_address_uf            = data.cum_address_uf
							
							let cum_use_users_id_access   = 0
							let query = 'INSERT INTO USE_USERS SET ?'
							let values = [{use_password: use_password, use_grp_group_id: use_grp_group_id}]
							
							pool.getConnection( (err, connection) => {
								connection.query(query, values, (error, results, fields) => {
									if (error) {
										connection.release()  
										return callback({status: 500, error: error})
									} else {
										query = "SELECT LAST_INSERT_ID()"
										connection.query(query, values, (error, results, fields) => {
											if (error) {
												connection.release()
												return callback({status: 500, error: error})
											} else {
												cum_use_users_id_access = results[0]['LAST_INSERT_ID()']
												query = 'INSERT INTO CUM_CUSTOMERS SET ?'
												values = [{cum_cpf: cum_cpf, cum_name: cum_name, cum_email: cum_email, cum_gender: cum_gender, cum_date_birth: cum_date_birth, cum_ddd: cum_ddd, cum_tel: cum_tel, cum_zip_code: cum_zip_code, cum_address_street: cum_address_street, cum_address_number: cum_address_number, cum_address_complement: cum_address_complement, cum_address_district: cum_address_district, cum_address_city: cum_address_city, cum_address_uf: cum_address_uf, cum_use_users_id_access: cum_use_users_id_access}]
												connection.query(query, values, (error, results, fields) => {
													if (error) {
														connection.release()  
														return callback({status: 500, error: error})
													}else{
														return callback(null, {status: 201, success: true})
													}
												})
											}
										})
									}
								})
							})
						}
					})
				})
			} else {
					return callback(null, {status: 401, success: false, error: 'CPF fora do formato padrão.'})
			}
		break;
	}
	return callback(null, {status: 404, success: false, error: 'Endpoint não encontrado.'})
}

function putId(route, data, callback) {
	return callback(null, {status: 404, success: false, error: 'Endpoint não encontrado.'})
}

function deleteId(cum_customers_id, callback) {
	return callback(null, {status: 404, success: false, error: 'Endpoint não encontrado.'})
}

function get (callback) {
	return callback(null, {status: 404, success: false, error: 'Endpoint não encontrado.'})
}

function getId (route, param, callback) {
	return callback(null, {status: 404, success: false, error: 'Endpoint não encontrado.'})
}

module.exports = {
		post,
		postId,
    putId,
    deleteId,
    get,
    getId
}