const externo   = require('./externo.js')
const verify   = require('../../verify.js')

function post (req, res, next) {
  externo.post (req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function postId (req, res, next) {
  externo.postId (req.params.id, req.body, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function putId (req, res, next) {
  externo.putId(req.params.id, req.body, (err, data) => {
      if (err) res.status(err.status).send(err)
      else {
        res.json(data)
      }
  })
}

function deleteId(req, res, next){
  externo.deleteId(req.params.id, (err, data) => {
    if (err) res.status(err.status).send(err)
    else {
      res.json(data)
    }
  })
}

function get (req, res, next) {
  externo.get( (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

function getId (req, res, next) {
  externo.getId(req.params.id, req.query, (err, data) => {
    if (err) {
      res.status(err.status).send(err)
    } else {
      res.json(data)
    }
  })
}

module.exports = {
  post,
  postId,
  putId,
  deleteId,
  get,
  getId
}
