function init() {

    const express 		   = require("express")
    const externoController   = require("./externoController.js")

    let routes = express.Router()

    routes.route('/')
        .post(
            externoController.post
        )

    routes.route('/')
        .get(
            externoController.get
        )
        
    routes.route('/:id/')
        .post(
            externoController.postId
        )
        .get(
            externoController.getId 
        )
        .put(
            externoController.putId
        )
        .delete(
            externoController.deleteId
        )

    return routes

}

module.exports = {
    init
}